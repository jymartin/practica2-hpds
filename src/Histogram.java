import java.util.HashMap;

public class Histogram {
    private int[] array;

    public Histogram(int[] array) {
        this.array = array;
    }

    public int[] getArray() {
        return array;
    }

    public HashMap<Integer, Integer> getHisto() {
        HashMap<Integer, Integer> histo = new HashMap<>();

        for (int key : array) {
            if (!histo.containsKey(key)) {
                histo.put(key, 0);
            }
            histo.put(key, histo.get(key) + 1);
        }
        return histo;
    }
}
